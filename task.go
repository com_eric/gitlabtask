package main

import "fmt"

type RangeList struct {
	dict       map[int]struct{}
	arrayValue []int
}

func halfFind(inputArray []int, target int) int {
	i, j := 0, len(inputArray)-1
	for i <= j {
		mid := i + (j-i)/2
		if inputArray[mid] == target {
			return mid
		}
		if inputArray[mid] > target {
			j = mid - 1
		} else {
			i = mid + 1
		}
	}
	return i
}

type insertStruct struct {
	index int
	value int
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	i := rangeElement[0]
	j := rangeElement[1]
	insertStructArray := []*insertStruct{}
	for m := i; m < j; m++ {
		if rangeList.dict == nil {
			rangeList.dict = make(map[int]struct{})
		}
		if _, ok := rangeList.dict[m]; ok {
			continue
		}
		afterIndex := halfFind(rangeList.arrayValue, m)
		insertStruct := &insertStruct{afterIndex, m}
		insertStructArray = append(insertStructArray, insertStruct)
	}
	copyArray := make([]int, len(rangeList.arrayValue)+len(insertStructArray))
	i, j, index := 0, 0, 0
	for ; i < len(rangeList.arrayValue) && j < len(insertStructArray); index++ {
		if i < insertStructArray[j].index {
			copyArray[index] = rangeList.arrayValue[i]
			rangeList.dict[rangeList.arrayValue[i]] = struct{}{}
			i++
			continue
		}
		rangeList.dict[insertStructArray[j].value] = struct{}{}
		copyArray[index] = insertStructArray[j].value
		j++
	}
	for ; i < len(rangeList.arrayValue); {
		copyArray[index] = rangeList.arrayValue[i]
		i++
		index++
	}
	for ; j < len(insertStructArray); {
		copyArray[index] = insertStructArray[j].value
		j++
		index++
	}
	rangeList.arrayValue = copyArray
	return nil
}
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	i := rangeElement[0]
	j := rangeElement[1]
	deleteArray := []int{}
	for m := i; m < j; m++ {
		if _, ok := rangeList.dict[m]; !ok {
			continue
		}
		curIndex := halfFind(rangeList.arrayValue, m)
		deleteArray = append(deleteArray, curIndex)
	}
	copyArray := make([]int, len(rangeList.arrayValue)-len(deleteArray))
	i, j, index := 0, 0, 0
	for ; i < len(rangeList.arrayValue) && j < len(deleteArray); i++ {
		if i == deleteArray[j] {
			delete(rangeList.dict, rangeList.arrayValue[i])
			j++
			continue
		}
		copyArray[index] = rangeList.arrayValue[i]
		index++
	}

	for ; i < len(rangeList.arrayValue); i++ {
		copyArray[index] = rangeList.arrayValue[i]
		index++
	}
	rangeList.arrayValue = copyArray
	return nil
}
func (rangeList *RangeList) Print() error {
	fmt.Println(rangeList.arrayValue)
	return nil
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
